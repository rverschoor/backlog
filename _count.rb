# frozen_string_literal: true

# Count ticket properties
class Count
  attr_reader :counts

  # Initialize the Count object
  # @param team [Team] has manager names for all agent IDs
  def initialize(team)
    @team = team
    init_counts
  end

  def count(ticket)
    return if devpulse? ticket

    count_status ticket
    # count_manager ticket
  end

  # Store timestamp in @counts :stamp
  # @param stamp [String]
  def stamp!(stamp)
    @counts[:stamp] = stamp
  end

  private

  # Set all @counts information to initial empty state
  def init_counts
    @counts = {}
    init_counts_stamp
    init_counts_status
    init_counts_manager
  end

  # Set @counts :stamp to nil
  def init_counts_stamp
    @counts[:stamp] = nil
  end

  # Set @counts :status to 0 for each status
  def init_counts_status
    @counts[:status] = {}
    STATUS.each do |status|
      @counts[:status][status] = {}
      @counts[:status][status][:assigned] = 0
      @counts[:status][status][:unassigned] = 0
    end
  end

  # Set @counts :manager to 0 for each manager
  def init_counts_manager
    @counts[:manager] = {}
    @team.managers.each do |manager|
      @counts[:manager][manager] = 0
    end
  end

  def devpulse?(ticket)
    (ticket['type'] == 'incident') && (ticket['status'] == 'hold') && ticket['tags'].include?('stage-other') && (ticket['tags'] & %w[waiting_on_bug waiting_on_feature_request waiting_on_rfh]).any?
  end

  def count_status(ticket)
    assigned = assigned?(ticket) ? :assigned : :unassigned
    @counts[:status][ticket['status']][assigned] += 1
  rescue StandardError => e
    puts e
    puts "ID:     #{ticket['id']}"
    puts "Status: #{ticket['status']}"
    puts "Assigned: #{assigned}"
  end

  # Is the ticket assigned?
  # @param ticket [Hash]
  # @return [Boolean]
  def assigned?(ticket)
    # Ticket is assigned if assignee_id is not nil
    ticket['assignee_id']
  end

  def count_manager(ticket)
    assignee_id = ticket['assignee_id']
    if assignee_id
      manager = @team.manager(assignee_id)
      if manager
        @counts[:manager][manager] += 1
      else
        puts "Ticket #{ticket['id']}: assignee has no maneger"
      end
    end
  end
end
