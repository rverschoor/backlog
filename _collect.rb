# frozen_string_literal: true

require_relative '_search'
require_relative '_count'

# Collect and process tickets
class Collect
  # Initialize the Collect object
  # @param team [Team] has managers for all agents
  def initialize(team)
    # [Search] Zendesk API calls to collect tickets
    @search = Search.new
    # [Count] counts tickets by status and manager
    @count = Count.new team
  end

  def process
    @count.stamp! Time.now.utc.iso8601
    STATUS.each do |status|
      process_tickets_with_status status
    end
    @count.counts
  end

  private

  def process_tickets_with_status(status)
    puts "Processing tickets with status #{status}"
    @search.tickets_with_status status do |data|
      tickets = data['results']
      process_tickets tickets
    end
  end

  def process_tickets(tickets)
    puts "Analyzing #{tickets.count} tickets"
    tickets.each do |ticket|
      @count.count ticket
    end
  end
end
