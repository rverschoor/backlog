# frozen_string_literal: true

require 'google/apis/sheets_v4'
require 'googleauth'

# Google Sheet API service
class Spreadsheet

  attr_reader :sheets

  def initialize(spreadsheet_id)
    @spreadsheet_id = spreadsheet_id
    init_google
    sanity_check
    @sheets = sheets_info # [{:id, :title}]
    @queue = init_request_queue
  end

  def values(sheet, range)
    begin
      result = @service.get_spreadsheet_values(@spreadsheet_id, "#{sheet}!#{range}")
    rescue Google::Apis::ClientError => e
      puts "Sheet API error occurred: #{e}"
      puts "Sheet: #{sheet}"
      puts "Range: #{range}"
      exit
    end
    result&.values
  end

  def nr_rows(sheet)
    # stoopid way to determine nr of rows, but I know no other way
    result = values sheet, 'A1:Z999999'
    result ? result.length : 0
  end

  # # values is an array
  def append_row(sheet, values)
    row = Google::Apis::SheetsV4::ValueRange.new
    row.values = [values]
    @service.append_spreadsheet_value(
      @spreadsheet_id,
      "#{sheet}!A1:A1",
      row,
      value_input_option: 'RAW'
    )
  end

  def add_sheets(titles)
    titles.each do |title|
      request = Google::Apis::SheetsV4::Request.new(
        add_sheet: {
          properties: { title: }
        }
      )
      add_to_queue request
    end
    response = run_queued
    update_add_sheet_info response
  end

  def update_add_sheet_info(response)
    response.replies.each do |reply|
      id = reply.add_sheet.properties.sheet_id
      title = reply.add_sheet.properties.title
      @sheets << { id:, title: }
    end
  end

  def rename_sheet(id, title, queue = nil)
    request = Google::Apis::SheetsV4::Request.new(
      update_sheet_properties: {
        fields: 'title',
        properties: {
          sheet_id: id,
          title:
        }
      })
    add_to_queue request
    run_queued unless queue == :queue
    update_rename_sheet_info id, title
  end

  def update_rename_sheet_info(id, title)
    index = @sheets.find_index { |sheet| sheet[:id] == id }
    @sheets[index] = { id:, title: }
  end

  # row is 1-based
  # values is an array of values
  def update_row(sheet, row_nr, values, queue = nil)
    validate_row_nr row_nr
    request = Google::Apis::SheetsV4::Request.new(
      update_cells: {
        start: { sheet_id: sheet, row_index: row_nr - 1, column_index: 0 },
        fields: 'userEnteredValue',
        rows: [{ values: [] }]
      })
    request.update_cells[:rows][0][:values] = values_to_update_entries values
    add_to_queue request
    run_queued unless queue == :queue
  end

  def run_queued
    # puts "run #{@queue.requests.length} queued requests"
    response = @service.batch_update_spreadsheet(@spreadsheet_id, @queue)
    @queue.requests.clear
    sleep 1 # Be nice to API
    response
  end

  def sheet_id(title)
    @sheets.find { |sheet| sheet[:title] == title }[:id]
  end

  private

  # Authenticate to use Google Spreadsheets
  def init_google
    create_credentials_file
    ENV['GOOGLE_APPLICATION_CREDENTIALS'] = "#{Dir.pwd}/credentials.json"
    scope = Google::Apis::SheetsV4::AUTH_SPREADSHEETS
    @service = Google::Apis::SheetsV4::SheetsService.new
    @service.authorization = Google::Auth.get_application_default(scope)
    delete_credentials_file
  end

  # Google expects that the GOOGLE_APPLICATION_CREDENTIALS environment variable
  # points to the credentials JSON file.
  # Instead of saving the credentials file in the repo, its content is base64
  # encoded and stored in an environment variable GOOGLE_APPLICATION_CREDENTIALS_BASE64
  # This method decodes the content and saves it into a credentials.json file, which
  # can then be used by Google::Auth.get_application_default
  def create_credentials_file
    base64 = ENV.fetch('GOOGLE_APPLICATION_CREDENTIALS_BASE64')
    File.open('credentials.json', 'w') do |f|
      f.write Base64.decode64(base64)
    end
  end

  # Delete the temporary credentials.json file
  def delete_credentials_file
    File.delete('credentials.json')
  end

  def sanity_check
    # Check if the spreadsheet exists and can be accessed
    @spreadsheet = @service.get_spreadsheet(@spreadsheet_id)
  rescue Google::Apis::ClientError => e
    puts "Error accessing spreadsheet: #{e.message}"
    puts 'sanity_check()'
    puts "Sheet: #{@spreadsheet_id}"
    exit
  end

  # Returns an array with sheet information
  # Array index == sheet index
  # Each array element contains {:id, :title}
  def sheets_info
    sheets = @service.get_spreadsheet(@spreadsheet_id).sheets
    info = []
    sheets.each do |sheet|
      info[sheet.properties.index] = {
        id: sheet.properties.sheet_id,
        title: sheet.properties.title,
      }
    end
    info
  end

  def init_request_queue
    queue = Google::Apis::SheetsV4::BatchUpdateSpreadsheetRequest.new
    queue.requests = []
    queue
  end

  def add_to_queue(request)
    @queue.requests << request
  end

  def validate_row_nr(row_nr)
    return if row_nr.positive?

    puts 'Row number must be > 0'
    exit
  end

  def values_to_update_entries(values)
    cells = []
    values.each do |value|
      case value
      when Integer, Float
        cells << { user_entered_value: { number_value: value } }
      when String
        cells << { user_entered_value: { string_value: value } }
      when TrueClass, FalseClass
        cells << { user_entered_value: { bool_value: value } }
      else
        puts 'Update entry, unhandled value type'
        puts "Value: #{value}"
        puts "Class: #{value.class}"
        exit
      end
    end
    cells
  end
end
