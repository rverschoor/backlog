# Backlog

Collect queue backlog metrics.

Count assigned and unassigned Support tickets that are New, Open, Pending, or On-Hold.

## Requirements

### Environment variables

- ZD_URL (https://gitlab.zendesk.com/api/v2)
- ZD_USERNAME
- ZD_TOKEN
- BACKLOG_SPREADSHEET_ID
- GOOGLE_APPLICATION_CREDENTIALS_BASE64

### Team info

The workdir must contain a `team` subdir, which is a clone of the `Support Team` project.\
Be sure to update this regularly.

```
git clone git@gitlab.com:gitlab-support-readiness/support-team.git
```

### Google credentials

Create service account and download credentials as JSON file.\
Copy file into app workdir, and rename to `credentials.json`.

### Google sheet

Create a spreadsheet.\
Share the spreadsheet with the service account.

Open the spreadsheet in Google Sheets.\
Extract the ID from the URL: https://docs.google.com/spreadsheets/d/`<this is the long ID string>`/edit#gid=0\
Copy the spreadsheet ID into the `BACKLOG_SPREADSHEET_ID` environment variable.

## Zendesk API ratelimit

Ticket information is collected through the `/search/export` endpoint.\
The normal `/search` endpoint can return a maximum of 1000 tickets, which is insuffiecient for
backlog reporting.\
The `/search/export` endpoint is unlimited in the number of tickets it can return.\
This comes at the price of a much lower ratelimit of 100 requests per minute per account,
compared to 2500 rpm for the normal search.

Currently approximately 20 calls are needed to collect all ticket information.\
The script uses a 1 second (configurable) delay between pagination calls.