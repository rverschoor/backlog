# frozen_string_literal: true

require 'yaml'

# Team info
class Team
  # Initialize the Team object
  # Parse the team YAML files and store info in @agents hash
  def initialize
    # [Hash<String agent ID, String manager name>]
    @agents = {}
    slurp files
  end

  # Lookup an agent's manager name
  # @param id [String] the agent ID
  # @return [String] the manager name
  def manager(id)
    @agents[id]
  end

  # Return all unique manager names
  # @return [Array<String>] of manager names
  def managers
    @agents.values.uniq
  end

  private

  # Loop through all team YAML files
  # @return [Array<String>] of file paths
  def files
    list = Dir['support-team/data/agents/**/*.yaml']
    unless list.length
      puts 'No agent YAML files found'
      exit
    end
    list
  end

  # Parse team YAML files
  # @param files [Array<String>] list of file paths to parse
  def slurp(files)
    files.each do |file|
      agent = YAML.safe_load_file(file, filename: file, permitted_classes: [Date])
      store agent
    end
  end

  # Store agent info in @agents hash
  # An agent can have have a Global and US Federal ID
  # For each ID, store the manager name
  # @param agent [Hash] agent info from YAML file
  def store(agent)
    zd_id_global = agent['zendesk']['main']['id']
    zd_id_usfed = agent['zendesk']['us-federal']['id']
    manager = agent['reports_to']
    @agents[zd_id_global] = manager
    @agents[zd_id_usfed] = manager if zd_id_usfed
  end
end
