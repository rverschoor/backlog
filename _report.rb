# frozen_string_literal: true

require_relative '_spreadsheet'

SHEETS = %w[Status Manager].freeze

# Report the collected counts
class Report
  def initialize
    @spreadsheet = initialize_spreadsheet
    initialize_sheets
  end

  # Report to console
  # Just a prettyprint
  def report_console(counts)
    puts '=== report ==='
    jj counts
  end

  # Report to Google sheet
  def report_spreadsheet(counts)
    puts '=== report spreadsheet ==='
    report_spreadsheet_status counts
  end

  private

  def initialize_spreadsheet
    spreadsheet_id = ENV.fetch('BACKLOG_SPREADSHEET_ID')
    Spreadsheet.new(spreadsheet_id)
  end

  def initialize_sheets
    return unless new_spreadsheet?

    rename_default_sheet
    add_extra_sheets
    add_headers
  end

  # Is this a new spreadsheet?
  # If there's just 1 sheet named 'Sheet1', it's a new spreadsheet
  def new_spreadsheet?
    @spreadsheet.sheets.length == 1 && @spreadsheet.sheets[0][:title] == 'Sheet1'
  end

  def rename_default_sheet
    @spreadsheet.rename_sheet 0, SHEETS[0]
  end

  def add_extra_sheets
    @spreadsheet.add_sheets SHEETS[1..]
  end

  def add_headers
    values = ['stamp']
    STATUS.each do |status|
      values << "#{status} assigned"
      values << "#{status} unassigned"
    end
    @spreadsheet.update_row @spreadsheet.sheet_id('Status'), 1, values
  end

  def report_spreadsheet_status(counts)
    status_counts = counts[:status]
    row = [counts[:stamp]]
    STATUS.each do |status|
      row << status_counts[status][:assigned]
      row << status_counts[status][:unassigned]
    end
    @spreadsheet.append_row 'Status', row
  end
end

