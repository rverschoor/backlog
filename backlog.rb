#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '_collect'
require_relative '_report'
require_relative '_team'

STATUS = %w[new open pending hold].freeze

# Main class
class Backlog
  # Initialize the object
  def initialize
    @team = Team.new
    @collect = Collect.new @team
    @report = Report.new
  end

  # Collect and count backlog information
  def process
    @counts = @collect.process
  end

  # Report on collected information
  def report
    # @report.report_console @counts
    @report.report_spreadsheet @counts
  end
end

# Start the app
app = Backlog.new
app.process
app.report
