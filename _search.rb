# frozen_string_literal: true

require_relative '_zendesk'

# Zendesk search class
class Search
  def initialize
    @zd = Zendesk.new page_size: 100, paginate_delay: 1, log_rate_limits: false
  end

  # Search unassigned tickets with a given status
  # @param status [String]
  # @yield [Hash] JSON result from Zendesk GET
  def unassigned_tickets_with_status(status, &block)
    tickets 'assignee:none', status, &block
  end

  # Search assigned tickets with a given status
  # @param status [String]
  # @yield [Hash] JSON result from Zendesk GET
  def assigned_tickets_with_status(status, &block)
    tickets '-assignee:none', status, &block
  end

  # Search tickets with a given status
  # @param status [String]
  # @yield [Hash] JSON result from Zendesk GET
  def tickets_with_status(status, &block)
    tickets '', status, &block
  end

  private

  # Search tickets with a given status
  # @param assigned [String] add API param 'assignee:none' or '-assignee:none'
  # @param status [String]
  # @yield [Hash] JSON result from Zendesk GET
  def tickets(assigned, status, &block)
    # Exclude non-Support tickets using tags
    tags = '-tags:ar_team -tags:billing_team -tags:accounts_receivable'
    url = "search/export?query=type:ticket #{tags} status:#{status} #{assigned}"
    @zd.get_paginate :cursor_pagination, url, { 'filter[type]': 'ticket' }, &block
  end

end
