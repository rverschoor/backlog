# frozen_string_literal: true

require 'faraday'
require 'faraday/retry'
require 'json'

# Zendesk API service
class Zendesk
  def initialize(page_size: 100, paginate_delay: 10, log_rate_limits: false, log_pagination_info: false)
    @page_size = page_size
    @paginate_delay = paginate_delay
    @log_rate_limits = log_rate_limits
    @log_pagination_info = log_pagination_info
    @data = nil
    @conn = Faraday.new(ENV.fetch('ZD_URL')) do |config|
      config.request :retry, retry_options
      config.response :json
      config.request :authorization, :basic, "#{ENV.fetch('ZD_USERNAME')}/token", ENV.fetch('ZD_TOKEN')
      # Log request
      # config.response :logger, nil, { headers: false, bodies: false }
    end
  end

  # GET request without pagination
  # @param endpoint [String] API endpoint
  # @param params [Hash] request params
  # @return [Array]
  def get(endpoint, params = nil)
    resp = @conn.get endpoint, params
    show_rate_limits resp if @log_rate_limits
    handle_status resp
    handle_body resp
  end

  # GET request with pagination
  # @param pagination_type [Symbol] :cursor_pagination or :offset_pagination
  # @param endpoint [String] API endpoint
  # @param params [Hash] request params
  # @yield [Array]
  def get_paginate(pagination_type, endpoint, params = nil)
    validate_pagination_type pagination_type
    url = endpoint
    loop do
      params = params_merge_page_size params
      @data = get url, params
      show_pagination_info if @log_pagination_info
      yield @data
      url = next_page
      sleep @paginate_delay if more?
      break unless more?
    end
  end

  private

  def more?
    case @pagination_type
    when :cursor_pagination
      @data['meta']['has_more'] # true/false
    when :offset_pagination
      @data['next_page'] # url/null
    else
      exit
    end
  end

  def next_page
    case @pagination_type
    when :cursor_pagination
      @data['links']['next']
    when :offset_pagination
      @data['next_page']
    else
      exit
    end
  end

  def validate_pagination_type(pagination_type)
    @pagination_type = pagination_type
    return if %i[cursor_pagination offset_pagination].include?(pagination_type)

    puts "Unknown pagination_type: #{pagination_type}"
    exit
  end

  def params_merge_page_size(params)
    case @pagination_type
    when :cursor_pagination
      page_size = { 'page[size]': @page_size}
    when :offset_pagination
      page_size = { per_page: @page_size}
    else
      exit
    end
    params ? params.merge(page_size) : page_size
  end

  def retry_options
    { retry_statuses: [429] } # Retry when hitting rate limit
  end

  def handle_body(resp)
    @data = resp.body
  end

  def handle_status(resp)
    return unless resp.status != 200

    puts "FAIL: Status #{resp.status}"
    exit 2
  end

  def show_pagination_info
    case @pagination_type
    when :cursor_pagination
      puts "has_more: #{more?}"
      puts "next_page: #{next_page}"
    when :offset_pagination
      puts "next_page: #{next_page}"
    else
      exit
    end
  end

  def show_rate_limits(resp)
    header = resp.env.response_headers
    print "x-rate-limit: #{header['x-rate-limit-remaining']}/#{header['x-rate-limit']}  "
    print "ratelimit-limit: #{header['ratelimit-remaining']}/#{header['ratelimit-limit']}  "
    puts "ratelimit-reset: #{header['ratelimit-reset']}"
  end
end
